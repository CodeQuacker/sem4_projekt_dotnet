﻿using Microsoft.AspNetCore.Identity;

namespace sem4_projekt_dotnet.Areas.Identity.Data;

public class sem4_projekt_dotnetUser : IdentityUser
{
    [PersonalData]
    public string name { get; set; }
    [PersonalData]
    public string surname { get; set; }
    [PersonalData]
    public string address { get; set; }
}