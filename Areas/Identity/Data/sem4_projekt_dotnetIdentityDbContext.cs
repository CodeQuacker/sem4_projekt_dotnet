using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Areas.Identity.Data;

public class sem4_projekt_dotnetIdentityDbContext : IdentityDbContext<sem4_projekt_dotnetUser>
{
    public DbSet<Item> Items { get; set; }
    public DbSet<Message> Messages { get; set; }
    public DbSet<Rate> Rates { get; set; }
    public DbSet<Intrest> Intrests  { get; set; }
    
    public sem4_projekt_dotnetIdentityDbContext(DbContextOptions<sem4_projekt_dotnetIdentityDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);
    }
}
