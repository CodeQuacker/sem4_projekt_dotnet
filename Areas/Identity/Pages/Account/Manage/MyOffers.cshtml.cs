using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Areas.Identity.Pages.Account.Manage
{
    [Authorize]
    public class MyOffersModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;
        private readonly UserManager<sem4_projekt_dotnetUser> _userManager;

        public MyOffersModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context, UserManager<sem4_projekt_dotnetUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IList<Item> Item { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Items != null)
            {
                Item = await _context.Items.Where(o => o.userId == _userManager.GetUserId(User)).ToListAsync();
            }
        }
    }
}
