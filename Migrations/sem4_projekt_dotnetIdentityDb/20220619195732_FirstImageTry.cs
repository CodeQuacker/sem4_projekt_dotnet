﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace sem4_projekt_dotnet.Migrations.sem4_projekt_dotnetIdentityDb
{
    public partial class FirstImageTry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "imagePath",
                table: "Items",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "imagePath",
                table: "Items");
        }
    }
}
