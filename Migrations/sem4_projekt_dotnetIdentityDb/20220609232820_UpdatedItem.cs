﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace sem4_projekt_dotnet.Migrations.sem4_projekt_dotnetIdentityDb
{
    public partial class UpdatedItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "category2",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "category3",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "category1",
                table: "Items",
                newName: "category");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "category",
                table: "Items",
                newName: "category1");

            migrationBuilder.AddColumn<string>(
                name: "category2",
                table: "Items",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "category3",
                table: "Items",
                type: "text",
                nullable: true);
        }
    }
}
