﻿using System.ComponentModel.DataAnnotations;

namespace sem4_projekt_dotnet.Models;

public class Item
{
    [Key] [Required] 
    public string itemId { get; set; } = Guid.NewGuid().ToString("N");

    [Display(Name = "User")]
    public string? userId { get; set; }

    [Required(ErrorMessage = "Item name is required")]
    [MinLength(3, ErrorMessage = "Item name is too short")]
    [Display(Name = "Item name")]
    public string name { get; set; }
    
    [Display(Name = "Main category")]
    public string? category { get; set; }

    [Display(Name = "Subcategory one")]
    public string? subCategory1 { get; set; }

    [Display(Name = "Subcategory two")]
    public string? subCategory2 { get; set; }

    [Required(ErrorMessage = "Recive address is required")]
    [Display(Name = "Address")]
    public string address { get; set; }

    [MaxLength(1000, ErrorMessage = "Description is too long")]
    [Display(Name = "Description")]
    public string description { get; set; }

    [Required(ErrorMessage = "Expiration date is required")]
    [Display(Name = "Expiration date")]
    public DateTime expirationDate { get; set; }

    [Display(Name = "Booked")] 
    public bool reservation { get; set; } = false;

    [Display(Name = "Booking user")]
    public string? reservationUserId { get; set; }

    [Display(Name = "Given")] 
    public bool handOverState { get; set; } = false;
    
    [Display(Name = "Image")]
    public string? imagePath { get; set; }
}