﻿using System.ComponentModel.DataAnnotations;

namespace sem4_projekt_dotnet.Models;

public class Intrest
{
    [Key]
    [Required]
    public string intrestId { get; set; } = Guid.NewGuid().ToString("N");
    
    [Display(Name = "Item")]
    public string? itemId { get; set; }
    
    [Display(Name = "User")]
    public string? userId { get; set; }
}