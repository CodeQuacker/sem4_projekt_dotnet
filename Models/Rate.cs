﻿using System.ComponentModel.DataAnnotations;

namespace sem4_projekt_dotnet.Models;

public class Rate
{
    [Key]
    [Required]
    [Display(Name = "Rate")]
    public string rateId { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required]
    [Display(Name = "User")]
    public string userId { get; set; }
    
    [Required]
    [Display(Name = "Item name")]
    public string itemId { get; set; }
    
    [Display(Name = "Mark")]
    public int mark { get; set; }
    
    [Display(Name = "Comment")]
    public string? comment { get; set; }
}