﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace sem4_projekt_dotnet.Models;

public class Message
{
    [Key]
    public string messageId { get; set; } = Guid.NewGuid().ToString("N");
    
    [Display(Name = "Sending user")]
    public string? userId { get; set; }
    
    [Display(Name = "Receiving user")]
    public string? reciveUserId { get; set; }
    
    [Display(Name = "Item name")]
    public string? itemId { get; set; }
    
    [Display(Name = "Message content")]
    [MaxLength(300, ErrorMessage = "Message is too long")]
    public string? messageContent { get; set; }
}