# Funkcjonalności

## Profil użytkownika

* [X] jako Niezalogowany Użytkownik mam możliwość rejestracji w serwisie (1pkt)
* [X] jako Niezalogowany Użytkownik mam możliwość zresetować moje hasło; procedura odzyskiwania hasła powinna być bezpieczna (1pkt)
* [X] jako Zalogowany Użytkownik mogę edytować moje dane (1pkt)
* [X] jako Niezalogowany Użytkownik mogę się zalogować i wylogować z serwisu (1pkt)

## Oferta

* [X] jako Zalogowany użytkownik mam możliwość publikowania oferty. Oferta zawiera zdjęcia danej rzeczy, opis, adres odbioru, datę wygaśnięcia ogłoszenia (2pkt)

* [X] jako Użytkownik (zalogowany i niezalogowany) mam możliwość dowolnego przeglądania aktualnych ofert (2pkt)
* [X] Oferta może być przypisana do kilku kategorii. Użytkownik (zalogowany i niezalogowany) mam możliwość przeglądania ofert według kategorii. (2pkt)
* [x] Użytkownik (zalogowany i niezalogowany) mam możliwość wyszukiwania ofert po kategorii lub dowolnej frazy lub lokalizacji (2 pkt)
* [X] jako Użytkownik mogę zgłosić zainteresowanie ofertą. Jedną ofertą może być zainteresowanych wielu użytkowników. Użytkownik , który jest właścicielem wystawionej rzeczy, może oznaczyć daną ofertę jako zarezerwowaną dla wybranego Użytkownika. Zarezerwowana oferta powinna być w widoczny sposób oznaczana na liście ofert. Właściciel oferty może odwołać rezerwację. (6pkt)
* [X] jako Właściciel oferty mogę oznaczyć ofertę jako zrealizowaną (1pkt)

## Ocena

* TODO
* [ ] jako Użytkownik, który otrzymał dany przedmiot, mogę ocenić Właściciela rzeczy (2pkt)
* [ ] jako Użytkownik, który otrzymał dany przedmiot, mogę zostawić komentarz na profilu Właściciela rzeczy (2pkt)

## Komunikacja

* [X] jako Zalogowany Właściciel rzeczy mogę komunikować się za pośrednictwem skrzynki nadawczo-odbiorczej z Użytkownikami zainteresowanymi ofertą (3pkt)

## Inne

* [X] estetyczny wygląd strony (1pkt)
