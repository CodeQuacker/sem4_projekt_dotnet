using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages.Items
{
    
    public class IndexModel : PageModel
    {
        public readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;
        public readonly UserManager<sem4_projekt_dotnetUser> _userManager;
        
        public IndexModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context, UserManager<sem4_projekt_dotnetUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        
        public string NameSort { get; set; }
        public string CategorySort { get; set; }
        public string SubCategory1Sort { get; set; }
        public string SubCategory2Sort { get; set; }
        public string ExpirationDateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CategoryFilter { get; set; }
        public string Subcategory1Filter { get; set; }
        public string Subcategory2Filter { get; set; }
        public string CurrentSort { get; set; }
        public string LocalizationFilter { get; set; }

        public IList<Item> Items { get;set; } = default!;

        public async Task OnGetAsync(string sortOrder, string searchString, string categoryString, string subcategory1String, string subcategory2String, string localizationString)
        {
            NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            CategorySort = string.IsNullOrEmpty(sortOrder) ? "category_desc" : "";
            SubCategory1Sort = string.IsNullOrEmpty(sortOrder) ? "subcategory1_desc" : "";
            SubCategory2Sort = string.IsNullOrEmpty(sortOrder) ? "subcategory2_desc" : "";
            ExpirationDateSort = sortOrder == "Date" ? "date_desc" : "Date";

            CurrentFilter = searchString;
            CategoryFilter = categoryString;
            Subcategory1Filter = subcategory1String;
            Subcategory2Filter = subcategory2String;
            LocalizationFilter = localizationString;

            IQueryable<Item> itemsIQ = from items in _context.Items select items;

            if (!string.IsNullOrEmpty(searchString))
            {
                itemsIQ = itemsIQ.Where(i => i.name.Contains(searchString) && i.handOverState == false);
            }
            if (!string.IsNullOrEmpty(localizationString))
            {
                itemsIQ = itemsIQ.Where(i => i.address.Contains(localizationString) && i.handOverState == false);
            }
            if (!string.IsNullOrEmpty(categoryString))
            {
                itemsIQ = itemsIQ.Where(i => i.category.Contains(categoryString) && i.handOverState == false);
            }
            if (!string.IsNullOrEmpty(subcategory1String))
            {
                itemsIQ = itemsIQ.Where(i => i.subCategory1.Contains(subcategory1String) && i.handOverState == false);
            }
            if (!string.IsNullOrEmpty(subcategory2String))
            {
                itemsIQ = itemsIQ.Where(i => i.subCategory2.Contains(subcategory2String) && i.handOverState == false);
            }
            
            switch (sortOrder)
            {
                case "name_desc":
                    itemsIQ = itemsIQ.OrderByDescending(itemsIQ => itemsIQ.name);
                    break;
                case "category_desc":
                    itemsIQ = itemsIQ.OrderByDescending(itemsIQ => itemsIQ.category);
                    break;
                case "subcategory1_desc":
                    itemsIQ = itemsIQ.OrderByDescending(itemsIQ => itemsIQ.subCategory1);
                    break;
                case "subcategory2_desc":
                    itemsIQ = itemsIQ.OrderByDescending(itemsIQ => itemsIQ.subCategory2);
                    break;
                case "Date":
                    itemsIQ = itemsIQ.OrderBy(itemsIQ => itemsIQ.expirationDate);
                    break;
                case "date_desc":
                    itemsIQ = itemsIQ.OrderByDescending(itemsIQ => itemsIQ.expirationDate);
                    break;
            }
            
            Items = await itemsIQ.AsNoTracking().Where(o => o.expirationDate > DateTime.Now.ToUniversalTime() && o.handOverState == false).ToListAsync();
            
        }
    }
}
