using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace sem4_projekt_dotnet.Pages.Items
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;

        private readonly UserManager<sem4_projekt_dotnetUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;


        public CreateModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context,
            UserManager<sem4_projekt_dotnetUser> userManager,IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty] public Item Item { get; set; } = default!;
        [BindProperty] public IFormFile Image { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            Item.userId = _userManager.GetUserId(User);
            Item.category = Request.Form["category"];
            Item.subCategory1 = Request.Form["subcategory1"];
            Item.subCategory2 = Request.Form["subcategory2"];
            Item.expirationDate = Item.expirationDate.ToUniversalTime();

            if (Image != null)
            {
                var fileName = GetUniqueName(Image.FileName);
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");
                var filePath = Path.Combine(uploads, fileName);
                Image.CopyTo(new FileStream(filePath,FileMode.Create));
                Item.imagePath = fileName;
            }
            
            _context.Items.Add(Item);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }

        private string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) 
                   + "_" + Guid.NewGuid().ToString().Substring(0, 10) 
                   + Path.GetExtension(fileName);
        }
    }
}