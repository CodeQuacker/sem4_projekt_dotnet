using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace sem4_projekt_dotnet.Pages.Items
{
    [Authorize]
    public class EditModel : PageModel
    {
        public readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;
        public readonly UserManager<sem4_projekt_dotnetUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public EditModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context,
            UserManager<sem4_projekt_dotnetUser> userManager,IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
        }

        [BindProperty] public Item Item { get; set; } = default!;
        [BindProperty] public string category { get; set; }
        [BindProperty] public string subcategory1 { get; set; }
        [BindProperty] public string subcategory2 { get; set; }
        [BindProperty] public IFormFile Image { get; set; }

        public List<Intrest> intrests = new List<Intrest>();
        public Dictionary<string, string> users = new Dictionary<string, string>();

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null || _context.Items == null)
            {
                return NotFound();
            }


            intrests = await _context.Intrests.ToListAsync();
            // users = await _userManager.Users.Include(u => intrests.Find(x => x.userId.Equals(u.Id)).userId);
            var item = await _context.Items.FirstOrDefaultAsync(m => m.itemId == id);
            if (item == null || item.userId != _userManager.GetUserId(User))
            {
                return NotFound();
            }
            Item = item;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            Item.category = category;
            Item.subCategory1 = subcategory1;
            Item.subCategory2 = subcategory2;
            Item.expirationDate = Item.expirationDate.ToUniversalTime();
            Item.userId = _userManager.GetUserId(User);

            if (Image != null)
            {
                var fileName = GetUniqueName(Image.FileName);
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");
                var filePath = Path.Combine(uploads, fileName);
                Image.CopyTo(new FileStream(filePath,FileMode.Create));
                Item.imagePath = fileName;
            }
            else
            {
                ;
            }
            _context.Attach(Item).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(Item.itemId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ItemExists(string id)
        {
            return (_context.Items?.Any(e => e.itemId == id)).GetValueOrDefault();
        }
        private string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) 
                   + "_" + Guid.NewGuid().ToString().Substring(0, 10) 
                   + Path.GetExtension(fileName);
        }
    }
}