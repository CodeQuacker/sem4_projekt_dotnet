﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages;
//TODO Dodać Stronę główną

public class IndexModel : PageModel
{
    private readonly ILogger<IndexModel> _logger;
    private readonly sem4_projekt_dotnetIdentityDbContext _context;

    [BindProperty]
    public string Search { get; set; }
    
    public IndexModel(ILogger<IndexModel> logger,sem4_projekt_dotnetIdentityDbContext context)
    {
        _logger = logger;
        _context = context;
    }

    public IList<Item> Items { get; set; } = default!;
    public async Task OnGetAsync()
    {

        if (_context.Items != null)
        {
            Items = await _context.Items.Where(o => o.expirationDate > DateTime.Now.ToUniversalTime() && o.handOverState == false).OrderBy(o=> o.expirationDate).Take(3).ToListAsync();
        }
    }

    public async Task<IActionResult> OnPostAsync()
    {
        string url = "/Items?SearchString=" + Search;
        return Redirect(url);
    }
}
