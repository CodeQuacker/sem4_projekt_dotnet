using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;
//TODO Należy zrobić cały system ocen !
namespace sem4_projekt_dotnet.Pages.Rates
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;

        public IndexModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context)
        {
            _context = context;
        }

        public IList<Rate> Rate { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Rates != null)
            {
                Rate = await _context.Rates.ToListAsync();
            }
        }
    }
}
