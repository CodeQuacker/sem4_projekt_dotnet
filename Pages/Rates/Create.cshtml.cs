using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages.Rates
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;

        public CreateModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Rate Rate { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid || _context.Rates == null || Rate == null)
            {
                return Page();
            }

            _context.Rates.Add(Rate);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
