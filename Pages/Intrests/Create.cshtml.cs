using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages.Intrests
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;
        private readonly UserManager<sem4_projekt_dotnetUser> _userManager;
        private Intrest output = new Intrest();
        
        public CreateModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context, UserManager<sem4_projekt_dotnetUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [BindProperty]
        public Intrest Intrest { get; set; } = default!;
        
        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null || _context.Items == null)
            {
                return NotFound();
            }

            string currentUser = _userManager.GetUserId(User);
            output.userId = currentUser;
            output.itemId = id;

            bool noWatchFlag = true;

            // var intrests =  await _context.Intrests.FirstOrDefaultAsync(m => m.itemId == id);
            var intrests =  await _context.Intrests.ToListAsync();
            foreach (Intrest intrest in intrests)
            {
                if (intrest.itemId == id && intrest.userId == currentUser)
                {
                    noWatchFlag = false;
                }
            }

            if (noWatchFlag)
            {
                _context.Intrests.Add(output);
                await _context.SaveChangesAsync();

                return RedirectToPage("./Index");
            }
            return RedirectToPage("../Items/Index");
            
        }
    }
}
