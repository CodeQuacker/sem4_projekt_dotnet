using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages.Intrests
{
    [Authorize]
    public class DeleteModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;

        public DeleteModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context)
        {
            _context = context;
        }

        [BindProperty]
      public Intrest Intrest { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null || _context.Intrests == null)
            {
                return NotFound();
            }

            var intrest = await _context.Intrests.FirstOrDefaultAsync(m => m.intrestId == id);

            if (intrest == null)
            {
                return NotFound();
            }
            else 
            {
                Intrest = intrest;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (id == null || _context.Intrests == null)
            {
                return NotFound();
            }
            var intrest = await _context.Intrests.FindAsync(id);

            if (intrest != null)
            {
                Intrest = intrest;
                _context.Intrests.Remove(Intrest);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
