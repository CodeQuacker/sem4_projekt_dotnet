using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages.Intrests
{
    public class ItemDetailsModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;

        public ItemDetailsModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context)
        {
            _context = context;
        }

      public Item Item { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null || _context.Items == null)
            {
                return NotFound();
            }

            var item = await _context.Items.FirstOrDefaultAsync(m => m.itemId == id);
            if (item == null)
            {
                return NotFound();
            }
            else 
            {
                Item = item;
            }
            return Page();
        }
    }
}
