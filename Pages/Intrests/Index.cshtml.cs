using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;
//TOOD znaleźć błąd i poprawić system zainteresowania (edycja przedmiotu !)
namespace sem4_projekt_dotnet.Pages.Intrests
{
    [Authorize]
    public class IndexModel : PageModel
    {
        public readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;
        public readonly UserManager<sem4_projekt_dotnetUser> _userManager;

        public IndexModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context,UserManager<sem4_projekt_dotnetUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IList<Intrest> Intrest { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Intrests != null)
            {
                Intrest = await _context.Intrests.Where(i => i.userId == _userManager.GetUserId(User)).ToListAsync();
            }
        }
    }
}
