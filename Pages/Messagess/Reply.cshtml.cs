﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages.Messagess
{
    [Authorize]
    public class Reply : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;
        private readonly UserManager<sem4_projekt_dotnetUser> _userManager;
        private string MessageId;
        public Reply(sem4_projekt_dotnetIdentityDbContext context, UserManager<sem4_projekt_dotnetUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [BindProperty] 
        public Message Message { get; set; } = default!;
        
        public IActionResult OnGetAsync(string id)
        {
            MessageId = id;
            return Page();
        }
        public async Task<IActionResult> OnPostAsync(string id)
        {
            var reciveUserId = _context.Messages.SingleOrDefault(m => m.messageId == id).userId;
            var sendUserId = _context.Messages.SingleOrDefault(m => m.messageId == id).reciveUserId;
            Message.itemId = _context.Messages.SingleOrDefault(m => m.messageId == id).itemId;
            Message.userId = sendUserId;
            Message.reciveUserId = reciveUserId;
            if (!ModelState.IsValid || _context.Messages == null || Message == null)
            {
                return Page();
            }

            _context.Messages.Add(Message);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}