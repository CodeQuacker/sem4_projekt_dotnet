using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using sem4_projekt_dotnet.Areas.Identity.Data;
using sem4_projekt_dotnet.Models;

namespace sem4_projekt_dotnet.Pages.Messagess
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext _context;
        private readonly UserManager<sem4_projekt_dotnetUser> _userManager;
        public string itemId;
        public CreateModel(sem4_projekt_dotnet.Areas.Identity.Data.sem4_projekt_dotnetIdentityDbContext context, UserManager<sem4_projekt_dotnetUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        
        [BindProperty] 
        public Message Message { get; set; } = default!;
        public IActionResult OnGetAsync(string id)
        {
            itemId = id;
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync(string id)
        {
            var userId = _userManager.GetUserId(User);
            var reciveUserId = _context.Items.SingleOrDefault(i => i.itemId == id).userId;
            Message.itemId = id;
            Message.userId = userId;
            Message.reciveUserId = reciveUserId;
          if (!ModelState.IsValid || _context.Messages == null || Message == null)
            {
                return Page();
            }

            _context.Messages.Add(Message);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
