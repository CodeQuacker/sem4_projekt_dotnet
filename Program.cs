using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.FileProviders;
using sem4_projekt_dotnet.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity.UI.Services;
using sem4_projekt_dotnet.Services;

var builder = WebApplication.CreateBuilder(args);


var connectionString = builder.Configuration.GetConnectionString("WebAppDb") ?? throw new InvalidOperationException("Connection string 'WebAppIdentityDb' not found.");

builder.Services.AddDbContext<sem4_projekt_dotnetIdentityDbContext>(options =>
    options.UseNpgsql(connectionString));;

builder.Services.AddDefaultIdentity<sem4_projekt_dotnetUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<sem4_projekt_dotnetIdentityDbContext>();;

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddTransient<IEmailSender, EmailSender>();
builder.Services.Configure<AuthMessageSenderOptions>(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.Services.CreateScope().ServiceProvider.GetRequiredService<sem4_projekt_dotnetIdentityDbContext>().Database.Migrate();

app.UseHttpsRedirection();
app.UseStaticFiles();

// var imagesPath = Path.GetFullPath(builder.Configuration.GetValue<string>("wwwroot/uploads"));
Directory.CreateDirectory("wwwroot/uploads");

// var imagesPath = Path.GetFullPath(builder.Configuration.GetValue<string>("ImagesDirectory"));
// Directory.CreateDirectory(imagesPath);
// app.UseStaticFiles(new StaticFileOptions {
//     FileProvider = new PhysicalFileProvider(imagesPath),
//     RequestPath = "/image/upload"
// });

app.UseRouting();
app.UseAuthentication();
app.UseStaticFiles();
app.UseAuthorization();

app.MapRazorPages();

app.Run();
