﻿
<h1>Użyte oprogramowanie</h1>

<h3>Uruchamianie środowiska :</h3>
- ``docker compose up --build``
- możliwe że przed uruchomieniem powyższej komendy trzeba usunąć folder volumes/ (jako superuser)

<h3>Realizacja aplikacji WWW :</h3> 
- Microsoft ASP.NET Core 6.0 
- C# 10.0
- Microsoft ASP.NET Razor 3.2.7

<h3>Baza danych :</h3>
- PostgreSQL 14.2
- pgAdmin 4

<h3>Framework’i :</h3>
-	Microsoft.AspNetCore.Identity.EntityFrameworkCore 6.0.5
-	Microsoft.AspNetCore.Identity.UI 6.0.5
-	Microsoft.EntityFrameworkCore 6.0.5
-	Microsoft.EntityFrameworkCore.Abstractions 6.0.5
-	Microsoft.EntityFrameworkCore.Analyzers 6.0.5 
-	Microsoft.EntityFrameworkCore.Design 6.0.5
-	Microsoft.EntityFrameworkCore.Relational 6.0.5
-	Microsoft.EntityFrameworkCore.Sqlite 6.0.5
-	Microsoft.EntityFrameworkCore.SqlServer 6.0.5
-	Microsoft.EntityFrameworkCore.Tools 6.0.5
-	Microsoft.VisualStudio.Web.CodeGeneration.Design 6.0.5
-	Npgsql.EntityFrameworkCore.PostgreSQL 6.0.4
-	SendGrid 9.28.0
