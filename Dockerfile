#  ===== PRODUCTION DOCKER =====
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app

# Copy everything
COPY . ./
# Restore as distinct layers
RUN dotnet restore
# Build and publish a release
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "sem4_projekt_dotnet.dll"]


#  ===== DEVELOPMENT DOCKER =====
#FROM mcr.microsoft.com/dotnet/sdk:6.0
#WORKDIR /app
#
## Copy everything
#COPY . ./
#RUN dotnet dev-certs https --trust
#RUN dotnet tool install --global dotnet-ef
#ENV PATH $PATH:/root/.dotnet/tools
#ENTRYPOINT [ "dotnet", "watch", "run" ]