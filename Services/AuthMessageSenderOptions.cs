﻿namespace sem4_projekt_dotnet.Services;

public class AuthMessageSenderOptions
{
    public string? SendGridKey { get; set; }
}